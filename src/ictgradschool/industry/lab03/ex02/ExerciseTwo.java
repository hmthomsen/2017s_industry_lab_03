package ictgradschool.industry.lab03.ex02;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */

/**
 * * Example usage of Keyboard package:
 *
 * String input = Keyboard.readInput();
 *
 * This will assign the line of text entered at the keyboard (as a String) to the input variable.
 *
 */


public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     *
     */

    /**
     * Enter a low and high
     * String lowNumber = Enter a low number
     * String highNumber = Enter a high number
     * Convert to integers
     * int lowInteger = lowNumber.parseInt
     * int highInteger = highNumber.parseInt
     *
     * int range = highInteger - lowInteger
     * randomNumber = math.Random * range
     * r1 = randomNumber + lowInteger
     * r2 = randomNumber + lowInteger
     * r3 = randomNumber + lowInteger
     * int SmallestNumb = Math.min(Math.min(r1,r2), r3)
     */

    private int printPromptGetIntResponse(String prompt_msg) {
        System.out.print(prompt_msg);
        String input_str = Keyboard.readInput();
        return Integer.parseInt(input_str);
    }

    private void start() {
        /*
        Enter the low and high numbers - in this case year of birth and current year
         */
        int lowInteger = printPromptGetIntResponse("Please enter a lower bound");
        int highInteger = printPromptGetIntResponse("Please enter an upper bound");

        System.out.println("You entered low: " + lowInteger + " and high: " + highInteger);
           /*
        Get range, 3 randoms in that range, and find the smallest
         */

        /*
        int value = (int)(Math.random()*(max - min)) + min;
         */
        int bestYear1 = getRandomIntInRangeInclusive(lowInteger, highInteger);
        int bestYear2 = getRandomIntInRangeInclusive(lowInteger, highInteger);
        int bestYear3 = getRandomIntInRangeInclusive(lowInteger, highInteger);

        System.out.println("Heres 3 numbers in that range - 1: " + bestYear1 + " 2: " + bestYear2 + " 3: " + bestYear3);


        int intBestEverYear = Math.min(bestYear1, Math.min(bestYear2,bestYear3));
        System.out.println("But the lowest was definitely " + intBestEverYear);

          }

    private int getRandomIntInRangeInclusive(int lowerBound, int upperBound) {
        return (int) ((Math.random() * (upperBound - lowerBound)) + lowerBound);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
