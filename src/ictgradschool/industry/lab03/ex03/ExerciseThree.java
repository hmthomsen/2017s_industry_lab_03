package ictgradschool.industry.lab03.ex03;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class ExerciseThree {

    private void start() {

        String sentence = getSentenceFromUser();

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return
     */
    private String getSentenceFromUser() {

        // TODO Prompt the user to enter a sentence, then get their input.
            System.out.println("What game shall we play?");
            System.out.println("POKER, CHESS, THERMONUCLEAR WAR?");
            String input_str = Keyboard.readInput();
            System.out.println("You chose: " + input_str);
        return input_str;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.

        int charactersInSentence = sentence.length();
        int randomPosition = (int)(Math.random() * charactersInSentence);
        return randomPosition;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {

        // TODO Implement this method

        String text = sentence;
        char a_char = text.charAt(position);
        System.out.println("The random character we're going to remove is " + a_char + " at position: " + position);

    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence, int position) {

        /**
         * Displaying the sentence with the character removed
         * String originalSentence = sentence;

         }

         */

        // TODO Implement this method
        char a_char = sentence.charAt(position);
        System.out.println(a_char);
        System.out.println(position);
        System.out.println(sentence);

        return null;



    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {

        // TODO Implement this method
    }

    public static void main(String[] args) {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}
